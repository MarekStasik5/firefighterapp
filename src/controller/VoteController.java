package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Incident;
import model.User;
import model.Vote;
import model.VoteType;
import service.IncidentService;
import service.VoteService;

@WebServlet("/vote")
public class VoteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User loggedUser=(User)request.getSession().getAttribute("user");
		if(loggedUser!=null) {
			VoteType voteType=VoteType.valueOf(request.getParameter("vote"));
			long userId=loggedUser.getId();
			long incidentId=Long.parseLong(request.getParameter("incident_id"));
			updateVote(userId, incidentId,voteType);
		}
		response.sendRedirect(request.getContextPath()+"/");
	}

	private void updateVote(long userId, long incidentId, VoteType voteType) {
		VoteService voteService = new VoteService();
		Vote existingVote=voteService.getVoteByIncidentUserId(incidentId, userId);
		Vote updatedVote=voteService.addOrUpdateVote(incidentId, userId, voteType);
		if(existingVote!=updatedVote||!updatedVote.equals(existingVote));
			updateIncident(incidentId, existingVote, updatedVote);
	}
	
	private void updateIncident(long incidentId, Vote oldVote, Vote newVote) {
		IncidentService discoveryService = new IncidentService();
		Incident discoveryById = discoveryService.getIncidentById(incidentId);
		Incident updatedDiscovery = null;
		if(oldVote == null && newVote != null) {
			updatedDiscovery = addIncidentVote(discoveryById, newVote.getVoteType());
		} else if(oldVote != null && newVote != null) {
			updatedDiscovery = removeDiscoveryVote(discoveryById, oldVote.getVoteType());
			updatedDiscovery = addIncidentVote(updatedDiscovery, newVote.getVoteType());
		}
		discoveryService.updateIncident(updatedDiscovery);
	}
	
	private Incident addIncidentVote(Incident incident, VoteType voteType) {
		Incident incidentCopy = new Incident(incident);
		if(voteType == VoteType.VOTE_UP) {
			incidentCopy.setUpVote(incidentCopy.getUpVote() + 1);
		} else if(voteType == VoteType.VOTE_DOWN) {
			incidentCopy.setDownVote(incidentCopy.getDownVote() + 1);
		}
		return incidentCopy;
	}
	
	private Incident removeDiscoveryVote(Incident incident, VoteType voteType) {
		Incident incidentCopy = new Incident(incident);
		if(voteType == VoteType.VOTE_UP) {
			incidentCopy.setUpVote(incidentCopy.getUpVote() - 1);
		} else if(voteType == VoteType.VOTE_DOWN) {
			incidentCopy.setDownVote(incidentCopy.getDownVote() - 1);
		}
		return incidentCopy;
	}

}
