package controller;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Incident;
import service.IncidentService;


@WebServlet("")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		saveIncidentsInRequest(request);
		request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	private void saveIncidentsInRequest(HttpServletRequest request) {
		IncidentService incidentService = new IncidentService();
		List<Incident> allIncidents = incidentService.getAllIncidents(new Comparator<Incident>() {
			//more votes = higher
			@Override
			public int compare(Incident d1, Incident d2) {
				int d1Vote = d1.getUpVote() - d1.getDownVote();
				int d2Vote = d2.getUpVote() - d2.getDownVote();
				if(d1Vote < d2Vote) {
					return 1;
				} else if(d1Vote > d2Vote) {
					return -1;
				}
				return 0;
			}
		});
		request.setAttribute("incidents", allIncidents);
	}
}
