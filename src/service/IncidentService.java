package service;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import DAO.DAOFactory;
import DAO.IncidentDAO;
import model.Incident;
import model.User;

public class IncidentService {
	public void addIncident(String name, String desc, User user) {
		Incident incident = createIncidentObject(name, desc, user);
		DAOFactory factory = DAOFactory.getDAOFactory();
		IncidentDAO incidentDao = factory.getIncidentDAO();
		incidentDao.create(incident);
	}
	
	private Incident createIncidentObject(String name, String desc,  User user) {
		Incident incident = new Incident();
		incident.setName(name);
		incident.setDescription(desc);
		User userCopy = new User(user);
		incident.setUser(userCopy);
		incident.setTimestamp(new Timestamp(new Date().getTime()));
		return incident;
	}
	
	public Incident getIncidentById(long incidentId) {
		DAOFactory factory = DAOFactory.getDAOFactory();
		IncidentDAO incidentDao = factory.getIncidentDAO();
		Incident incident = incidentDao.read(incidentId);
		return incident;
	}
	
	public boolean updateIncident(Incident incident) {
		DAOFactory factory = DAOFactory.getDAOFactory();
		IncidentDAO incidentDao = factory.getIncidentDAO();
		boolean result = incidentDao.update(incident);
		return result;
	}
	
	public List<Incident> getAllIncidents() {
		return getAllIncidents(null);
	}
	
	public List<Incident> getAllIncidents(Comparator<Incident> comparator) {
		DAOFactory factory = DAOFactory.getDAOFactory();
		IncidentDAO incidentDao = factory.getIncidentDAO();
		List<Incident> incidents = incidentDao.getAll();
		if(comparator != null && incidents != null) {
			incidents.sort(comparator);
		}
		return incidents;
	}
}