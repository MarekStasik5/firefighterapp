package service;

import java.sql.Timestamp;
import java.util.Date;

import DAO.DAOFactory;
import DAO.VoteDAO;
import model.Vote;
import model.VoteType;

public class VoteService {
	
	public Vote addVote(long incidentId, long userId, VoteType voteType) {
		Vote vote = new Vote();
		vote.setIncidentId(incidentId);
		vote.setUserId(userId);
		vote.setDate(new Timestamp(new Date().getTime()));
		vote.setVoteType(voteType);
		DAOFactory factory = DAOFactory.getDAOFactory();
		VoteDAO voteDao = factory.getVoteDAO();
		vote = voteDao.create(vote);
		return vote;
	}
	
	public Vote updateVote(long incidentId, long userId, VoteType voteType) {
		DAOFactory factory = DAOFactory.getDAOFactory();
		VoteDAO voteDao = factory.getVoteDAO();
		Vote voteToUpdate = voteDao.getVoteByUserIdIncidentId(userId, incidentId);
		if(voteToUpdate != null) {
			voteToUpdate.setVoteType(voteType);
			voteDao.update(voteToUpdate);
		}
		return voteToUpdate;
	}
	
	public Vote addOrUpdateVote(long incidentId, long userId, VoteType voteType) {
		DAOFactory factory = DAOFactory.getDAOFactory();
		VoteDAO voteDao = factory.getVoteDAO();
		Vote vote = voteDao.getVoteByUserIdIncidentId(userId, incidentId);
		Vote resultVote = null;
		if(vote == null) {
			resultVote = addVote(incidentId, userId, voteType);
		} else {
			resultVote = updateVote(incidentId, userId, voteType);
		}
		return resultVote;
	}
	
	public Vote getVoteByIncidentUserId(long incidentId, long userId) {
		DAOFactory factory = DAOFactory.getDAOFactory();
		VoteDAO voteDao = factory.getVoteDAO();
		Vote vote = voteDao.getVoteByUserIdIncidentId(userId, incidentId);
		return vote;
	}
}
