package DAO;

import model.Vote;

public interface VoteDAO extends GenericDAO<Vote, Long> {
	public Vote getVoteByUserIdIncidentId(long userId, long incidentId);

}
