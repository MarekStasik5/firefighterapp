package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import model.Incident;
import model.User;
import util.ConnectionProvider;


public class IncidentDAOImpl implements IncidentDAO {

	private static final String CREATE_INCIDENT = 
			  "INSERT INTO incident(name, description, user_id, date, up_vote, down_vote) "
			  + "VALUES(:name, :description, :user_id, :date, :up_vote, :down_vote);";
	private static final String READ_ALL_INCIDENTS = 
			  "SELECT user.user_id, username, email, is_active, password, incident_id, name, description,  date, up_vote, down_vote "
			  + "FROM incident LEFT JOIN user ON incident.user_id=user.user_id;";
	private static final String READ_INCIDENT = 
				"SELECT user.user_id, username, email, is_active, password, incident_id, name, description, date, up_vote, down_vote "
				+ "FROM incident LEFT JOIN user ON incident.user_id=user.user_id WHERE incident_id=:incident_id;";
	private static final String UPDATE_INCIDENT = 
				"UPDATE incident SET name=:name, description=:description, user_id=:user_id, date=:date, up_vote=:up_vote, down_vote=:down_vote "
				+ "WHERE incident_id=:incident_id;";

	private NamedParameterJdbcTemplate template;
			
	public IncidentDAOImpl() {
		template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
	}

	@Override
	public Incident create(Incident incident) {
		Incident resultDiscovery = new Incident(incident);
		KeyHolder holder = new GeneratedKeyHolder();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("description", incident.getDescription());
		paramMap.put("name", incident.getName());
		paramMap.put("user_id", incident.getUser().getId());
		paramMap.put("date", incident.getTimestamp());
		paramMap.put("up_vote", incident.getUpVote());
		paramMap.put("down_vote", incident.getDownVote());
		SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
		int update = template.update(CREATE_INCIDENT, paramSource, holder);
			if(update > 0) {
				resultDiscovery.setId((Long)holder.getKey());
			}
			return resultDiscovery;
	}

	@Override
	public Incident read(Long primaryKey) {
		SqlParameterSource paramSource = new MapSqlParameterSource("incident_id", primaryKey);
			Incident incident = template.queryForObject(READ_INCIDENT, paramSource, new IncidentRowMapper());
			return incident;
	}

	@Override
	public boolean update(Incident incident) {
		boolean result = false;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("incident_id", incident.getId());
		paramMap.put("name", incident.getName());
		paramMap.put("description", incident.getDescription());
		paramMap.put("user_id", incident.getUser().getId());
		paramMap.put("date", incident.getTimestamp());
		paramMap.put("up_vote", incident.getUpVote());
		paramMap.put("down_vote", incident.getDownVote());
		SqlParameterSource paramSource = new MapSqlParameterSource(paramMap);
		int update = template.update(UPDATE_INCIDENT, paramSource);
		if(update > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean delete(Long key) {
		return false;
	}

	@Override
	public List<Incident> getAll() {
		List<Incident> discoveries = template.query(READ_ALL_INCIDENTS, new IncidentRowMapper());
		return discoveries;
	}
			
	private class IncidentRowMapper implements RowMapper<Incident> {
		@Override
		public Incident mapRow(ResultSet resultSet, int row) throws SQLException {
			Incident incident = new Incident();
			incident.setId(resultSet.getLong("incident_id"));
			incident.setName(resultSet.getString("name"));
			incident.setDescription(resultSet.getString("description"));
			incident.setUpVote(resultSet.getInt("up_vote"));
			incident.setDownVote(resultSet.getInt("down_vote"));
			incident.setTimestamp(resultSet.getTimestamp("date"));
			User user = new User();
			user.setId(resultSet.getLong("user_id"));
			user.setUsername(resultSet.getString("username"));
			user.setEmail(resultSet.getString("email"));
			user.setPassword(resultSet.getString("password"));
			incident.setUser(user);
			return incident;
		}
	}
	
}
