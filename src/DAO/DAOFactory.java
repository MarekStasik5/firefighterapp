package DAO;

import exception.NoSuchDbTypeException;

public abstract class DAOFactory {
	public abstract IncidentDAO getIncidentDAO();
	public abstract UserDAO getUserDAO();
	public abstract VoteDAO getVoteDAO();
	
	public static final int MYSQL_DAO_FACTORY=1;
	
	public static DAOFactory getDAOFactory() {
		DAOFactory factory=null;
		try {
			factory=getDAOFactory(MYSQL_DAO_FACTORY);
		}catch(NoSuchDbTypeException e) {
			e.printStackTrace();
		}
		return factory;
	}
	
	public static DAOFactory getDAOFactory(int type) throws NoSuchDbTypeException{
		switch(type) {
		case MYSQL_DAO_FACTORY:
			return new MySQLDAOFactory();
		default:
			throw new NoSuchDbTypeException();
		}
	}
}
