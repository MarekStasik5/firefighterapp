package DAO;

public class MySQLDAOFactory extends DAOFactory{

	@Override
	public IncidentDAO getIncidentDAO() {
		return new IncidentDAOImpl();
	}

	@Override
	public UserDAO getUserDAO() {
		return new UserDAOImpl();
	}

	@Override
	public VoteDAO getVoteDAO() {
		return new VoteDAOImpl();
	}
	
}
