package DAO;

import java.util.List;

import model.Incident;

public interface IncidentDAO extends GenericDAO<Incident, Long> {
	List<Incident>getAll();

}
